-- Small script to generate json files for lifomapserver to use for routing

advtrains = {}
minetest = {}
core = minetest

math.hypot = function(a,b) return math.sqrt(a*a + b*b) end
function attrans(str) return str end

dofile("vector.lua")
local serialize = dofile("serialize.lua")
dofile("helpers.lua")


function parse_args(argv) 
	local i = 1
	local no_trains = false
	local datapath, mappath, worldimage
	while i <= #argv do
		local a = argv[i]
		if (a == "-m") or (a == "--map-file") then
			-- only draw trains – requires specifying an already drawn file
			i = i+1
			if not argv[i] then
				error(("missing filename after `%s'"):format(a))
			end
			mappath = argv[i]
		elseif (a == "-t") or (a == "--no-trains") then
			-- do not draw trains
			no_trains = true
		elseif (a == "-w") or (a == "--world-image") then
			-- overlay over world image
			i = i+1
			if not argv[i] then
				error(("missing filename after `%s'"):format(a))
			end
			worldimage = argv[i]
		else
			datapath = a
		end
		
		i = i + 1
	end
	return datapath, mappath, no_trains, worldimage
end

datapath, mappath, no_trains, worldimage = parse_args(arg)

function ars_to_text(arstab)
	if not arstab then
		return "{}"
	end
	
	local txt = {}
	local ln = {}
	local rc = {}
	for i, arsent in ipairs(arstab) do
		local n = ""
		if arsent.n then
			n = "!"
		end
		if arsent.ln then
			ln[#ln+1] = '"'..n..arsent.ln..'"'
		elseif arsent.rc then
			rc[#ln+1] = '"'..n..arsent.rc..'"'
		elseif arsent.c then
			txt[#txt+1] = "#"..arsent.c
		end
	end

	return '{"LN": ['..table.concat(ln,',')..'], "RC" : ['..table.concat(rc,",")..']'.. ',"default": '..(arstab.default and "true" or "false").."}\n"
end


--local file = io.open(datapath.."advtrains_interlocking_tcbs", "r")
--local tbl = minetest.deserialize(file:read("*a"))
local tbl = serialize.read_from_file(datapath.."advtrains_interlocking")
advtrains.tcbs = tbl.tcbs
--file:close()

local jsonfile = io.open(datapath.."signals.json", "w")


tcbstr = {}
for k,v in pairs(advtrains.tcbs) do
	local routes = v[1].routes or {}
	if v[2].routes then
		for a,b in ipairs(v[2].routes) do
			routes[#routes+1] = b
		end
	end
	local pos = k:sub(2,-2)
	local sp = {}
	for j in (pos..","):gmatch("([^,]+),") do
		sp[#sp+1] = j
	end
		
	local tcbsidestr = {}
	local hasroutes = false
	for s=1,2 do
		local routestr = {}
		local side = v[s]
		local routes = side.routes or {}
		local signame = side.signal_name or ""
		local auto = nil

		for i,r in pairs(routes) do
			hasroutes = true

			local tcbps = {}
			local tcbs = {}
			--		svgfile:write("<circle cx=\""..sp[1].."\" cy=\""..-sp[3].."\" r=\"3\" stroke=\"".."purple".."\" stroke-width=\"1\" fill=\"none\" />")
			for ind,ps in ipairs(r) do
				if ps.next then
					local tcb = ps.next.p
					--				print(minetest.serialize(ps.next))
					if tcb and tcb.x then
						tcbps[#tcbps+1] = tcb.x..","..tcb.y..","..tcb.z
						tcbs[#tcbs+1] = ps.next.s
					end
				end
			end
			local auto = '"auto": false'
			if side.routeset and i == side.routeset and side.route_auto then
				auto = '"auto": true'
			end
			if #tcbps > 0 then
				routestr[#routestr+1] = '{ "name": "'..r.name..'",\n"endpoint": "'..tcbps[#tcbps]..'",\n"ars": '..ars_to_text(r.ars)..","..auto..', "endpoint_side": '..tcbs[#tcbs]..' }'
			end
		end
		tcbsidestr[#tcbsidestr+1] = '{ "routes": [ '..table.concat(routestr, ",\n")..' ], "signal_name": "'..signame..'"}'
	end
	if hasroutes then
		tcbstr[#tcbstr+1] = '"'..pos..'": { "type" : "Feature", "geometry": { "type": "Point", "coordinates": [ '..sp[1]..","..sp[3]..']}, "properties" : { "pos": "'..pos..'", "sides" : [\n'..table.concat(tcbsidestr,",\n").."]}}"
	end
end

jsonfile:write("{"..table.concat(tcbstr,",\n").."}")
jsonfile:close()
